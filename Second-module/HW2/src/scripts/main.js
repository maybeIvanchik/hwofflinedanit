"use strict";

const header = document.querySelector(".header-page");
const navbarList = document.querySelector(".navbar__list_dropdown");
const navbarBtn = document.querySelector(".navbar__button");

header.addEventListener("click", (event) => {
  if (!event.target.closest(".navbar__button")) {
    if (event.target.closest(".navbar__list")) {
      navbarBtn.classList.add("anim");
      setTimeout(() => {
        document.querySelector(".navbar__button").classList.remove("anim");
        // if()
        navbarList.classList.remove("active");
      }, 200);
      setTimeout(() => {
        navbarList.classList.remove("active");
        document
          .querySelector(".header-page__open-icon")
          .classList.toggle("active");
        document
          .querySelector(".header-page__close-icon")
          .classList.toggle("active");
      }, 550);
    } else if (navbarList.classList.contains("active")) {
      navbarBtn.classList.add("anim");
      document
        .querySelector(".header-page__open-icon")
        .classList.toggle("active");
      document
        .querySelector(".header-page__close-icon")
        .classList.toggle("active");
      setTimeout(() => {
        document.querySelector(".navbar__button").classList.remove("anim");
        // if()
        navbarList.classList.remove("active");
      }, 200);
    } else {
      return;
    }
  }
});

navbarBtn.addEventListener("click", () => {
  navbarBtn.classList.add("anim");
  document.querySelector(".header-page__open-icon").classList.toggle("active");
  document.querySelector(".header-page__close-icon").classList.toggle("active");
  setTimeout(() => {
    document.querySelector(".navbar__button").classList.remove("anim");
    // if()
    navbarList.classList.toggle("active");
  }, 200);
});
