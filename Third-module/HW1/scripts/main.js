"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(newName) {
    return (this._name = newName);
  }
  set age(newAge) {
    return (this._age = newAge);
  }
  set salary(newSalary) {
    return (this._salary = newSalary);
  }
}
const employee = new Employee("John", 21, 2000);
console.log(employee);

class Programmer extends Employee {
  constructor(lang, ...args) {
    super(...args);
    this.lang = lang;
  }

  get name() {
    return super.name;
  }
  get age() {
    return super.age;
  }
  get salary() {
    return super.salary * 3;
  }

  set name(newName) {
    return (super.name = newName);
  }
  set age(newAge) {
    return (super.age = newAge);
  }
  set salary(newSalary) {
    return (super.salary = newSalary);
  }
}
const programmerFirst = new Programmer(
  ["English", "Germany"],
  "Klif",
  42,
  4000
);
console.log(programmerFirst);
const programmerSecond = new Programmer(
  ["English", "Ukrainian"],
  "Boria",
  31,
  2500
);
console.log(programmerSecond);
const programmerThird = new Programmer(
  ["English", "Germany", "Russian"],
  "Kolia",
  19,
  1100
);
console.log(programmerThird);
