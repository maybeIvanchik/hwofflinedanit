class cardsContent {
  constructor(title, text, id, postId) {
    this.title = title;
    this.text = text;
    this.id = id;
    this.postId = postId;
  }

  render(selector) {
    document.querySelector(`.${selector}-${this.id}`).insertAdjacentHTML(
      "beforeend",
      `<div class="middle-bar__cont-text middle-bar__cont-text">
      <h2 class="middle-bar__title">${this.title}</h2>
      <p class="middle-bar__text">${this.text}</p>
      <button class="middle-bar__dlt-btn middle-bar__dlt-btn-${this.postId}">Delete</button>
      </div>`
    );
  }
}

export default cardsContent;
