import Card from "./card.js";
import cardsContent from "./cardsContent.js";

Promise.all([
  fetch("https://ajax.test-danit.com/api/json/users", {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  }).then((res) => res.json()),
  fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  }).then((res) => res.json()),
])
  .then((res) => {
    const [users, posts] = res;
    console.log(users);
    console.log(posts);
    users.forEach(({ id, name, username, email }) => {
      new Card(name, username, email, id).renderUser(
        ".middle-bar__list",
        "middle-bar__item"
      );

      posts.forEach(({ title, body, userId, id: postsId }) => {
        if (userId === id) {
          new cardsContent(title, body, id, postsId).render("middle-bar__item");
        } else {
          return;
        }
      });
    });
  })
  .then(() => {
    document
      .querySelector(".middle-bar__list")
      .addEventListener("click", (e) => {
        if (!e.target.closest(".middle-bar__dlt-btn")) return;

        let idOfPost = e.target
          .closest(".middle-bar__dlt-btn")
          .className.split("btn-")[1];

        fetch(`https://ajax.test-danit.com/api/json/posts/${idOfPost}`, {
          method: "DELETE",
        })
          .then(() => {
            e.target
              .closest(".middle-bar__dlt-btn")
              .parentNode.classList.add("anim");
            setTimeout(() => {
              e.target.closest(".middle-bar__dlt-btn").parentNode.remove();
            }, 340);
          })
          .catch((err) => {
            console.log(err);
          });
      });
  })
  .catch((err) => {
    console.log(err);
  });
