class Card {
  constructor(name, userName, email, id) {
    this.name = name;
    this.userName = userName.toLowerCase();
    this.email = email.toLowerCase();
    this.id = id;
  }

  renderUser(selector, childrenSelector) {
    document.querySelector(selector).insertAdjacentHTML(
      "beforeend",
      `<li class="${childrenSelector} ${childrenSelector}-${this.id}">
      <div class="middle-bar__cont">
      <p class="middle-bar__name">${this.name}</p>
    <p class="middle-bar__username">@${this.userName}</p>
    <p class="middle-bar__email">${this.email}</p>
    </div>`
    );
  }
}

export default Card;

{
  /* <h2 class="middle-bar__title">${this.title}</h2>
    <p class="middle-bar__text">${this.text}</p></li> */
}
