const ipBtn = document.querySelector(".ip-box__btn");

ipBtn.addEventListener("click", async () => {
  ipBtn.setAttribute("disabled", true);
  document.querySelector(".ip-box").insertAdjacentHTML(
    "beforeend",
    ` <ul class="ip-box__list">
    <li class="ip-box__item">
      <p class="ip-box__cont">Сontinent:</p>
      <p class="ip-box__cont-info">
        <div class="ip-box__loading"></div>
      </p>
    </li>
    <li class="ip-box__item">
      <p class="ip-box__country">
        Сountry:
    </p>
    <p class="ip-box__country-info">
        <div class="ip-box__loading"></div>
    </p>
    </li>
    <li class="ip-box__item">
      <p class="ip-box__region">
        Region:
    </p>
    <p class="ip-box__region-info"
      ><div class="ip-box__loading"></div>
    </p>
    </li>
    <li class="ip-box__item">
      <p class="ip-box__city">
        City:
    </p>
    <p class="ip-box__city-info"
      ><div class="ip-box__loading"></div>
    </p>
    </li>
    <li class="ip-box__item">
      <p class="ip-box__district">
        District:
    </p>
    <p class="ip-box__district-info"
      ><div class="ip-box__loading"></div>
    </p>
    </li>
  </ul>`
  );

  const { ip } = await fetch("https://api.ipify.org/?format=json")
    .then((res) => res.json())
    .catch((err) => console.warn(err));

  const { continent, country, regionName, city, district } = await fetch(
    `http://ip-api.com/json/${ip}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query`
  )
    .then((res) => res.json())
    .catch((err) => console.warn(err));

  const newArr = [continent, country, regionName, city, "undefined"];
  let i = 0;
  document.querySelectorAll(".ip-box__loading").forEach((el) => {
    el.insertAdjacentHTML(
      "beforebegin",
      `<p style="margin-left: 10px;">${newArr[i]}</p>`
    );
    el.remove();
    i++;
  });
});
