"use strict";

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const listContainer = document.querySelector("#root");

class WithoutProp extends Error {
  constructor(value, index) {
    super();
    this.name = `Missing property`;
    this.message = `In book with number ${index} missing property - "${value.toUpperCase()}"`;
  }
}

books.forEach((elem, index) => {
  let numOfBook = index + 1;
  try {
    if (Object.keys(elem).length < 3) {
      throw new WithoutProp();
    } else {
      listContainer.insertAdjacentHTML(
        "beforeend",
        `<ul class="list"><li class="title">Book ${numOfBook}</li></ul>`
      );
      for (const key in elem) {
        document
          .querySelectorAll("ul")
          [document.querySelectorAll("ul").length - 1].insertAdjacentHTML(
            "beforeend",
            `<li>${key.charAt(0).toUpperCase() + key.slice(1)} - ${
              elem[key]
            }</li>`
          );
      }
    }
  } catch {
    if (!elem.hasOwnProperty("author")) {
      console.log(new WithoutProp("author", numOfBook));
    } else if (!elem.hasOwnProperty("name")) {
      console.log(new WithoutProp("name", numOfBook));
    } else if (!elem.hasOwnProperty("price")) {
      console.log(new WithoutProp("price", numOfBook));
    }
  }
});
