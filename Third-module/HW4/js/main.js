"use strict";

const starWarsUrl = "https://ajax.test-danit.com/api/swapi/films";

fetch(starWarsUrl)
  .then((res) => res.json())
  .then((res) => {
    res.sort((a, b) => {
      return a.episodeId - b.episodeId;
    });
    res.forEach(({ episodeId, name, openingCrawl, characters }) => {
      document.querySelector(".body-page").insertAdjacentHTML(
        "beforeend",
        ` <div class="film-card">
      <h2 class="film-card__title">${name}</h2>
      <p class="film-card__id">Episode: ${episodeId}</p>
      <p class="film-card__desc">${openingCrawl}</p>
      <ul class="film-card__num film-card__num_${episodeId}"><div class="film-card__placeholder"><div class="activity"></div></div></ul>
    </div>`
      );
      const charactersArr = characters.map((elem) =>
        fetch(elem).then((response) => response.json())
      );
      Promise.allSettled(charactersArr).then((result) => {
        document.querySelector(`.film-card__num_${episodeId}`).innerHTML = "";
        result.forEach((elem) => {
          const {
            value: { name },
          } = elem;
          if (elem.status === "fulfilled") {
            document
              .querySelector(`.film-card__num_${episodeId}`)
              .insertAdjacentHTML("beforeend", `<li>${name}</li>`);
          }
        });
      });
    });
  });
