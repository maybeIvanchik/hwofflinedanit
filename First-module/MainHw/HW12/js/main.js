"use strict";

const keyboardBtn = document.querySelectorAll(".btn");

window.addEventListener("keyup", (event) => {
  keyboardBtn.forEach((btn) => {
    if (
      event.key.toUpperCase() === btn.textContent ||
      event.key === btn.textContent
    ) {
      keyboardBtn.forEach((btn) => {
        btn.classList.remove("active");
      });
      btn.classList.add("active");
    }
  });
});
