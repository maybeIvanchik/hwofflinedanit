"use strict";

function createNewUser() {
  let firstName = prompt("Enter your name");
  let lastName = prompt("Enter your lastName");

  const newUser = {
    firstName: firstName,
    lastName: lastName,
  };

  newUser.getLogin = () => {
    return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase();
  };

  console.log(newUser.getLogin());
  return newUser;
}

console.log(createNewUser());
