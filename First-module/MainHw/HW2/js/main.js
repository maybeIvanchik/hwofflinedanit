"use strict";

let userAge, userName;

do {
  userName = prompt("Print your name:");
  userAge = prompt("Print your age:");
} while (
  userName === "" ||
  userName === null ||
  isNaN(userAge) ||
  userAge === "" ||
  userAge === null
);

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  let choiseCheck = confirm("Are you sure you want to continue?");

  if (choiseCheck) {
    alert(`Welcome ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else if (userAge > 22) {
  alert(`Welcome ${userName}`);
}
