"use strict";
let firstNumber, secondNumber;

do {
  firstNumber = prompt("Print here your first number");
  secondNumber = prompt("Print here your second number");
} while (
  isNaN(firstNumber) ||
  isNaN(secondNumber) ||
  firstNumber === null ||
  secondNumber === null ||
  firstNumber === "" ||
  secondNumber === ""
);
let mathOperation = prompt("Print here your math operation");

function calcNUm(firstNum, secondNum, mathOper) {
  switch (mathOper) {
    case "+":
      return +firstNum + +secondNum;
    case "-":
      return firstNum - secondNum;
    case "/":
      return firstNum / secondNum;
    case "*":
      return firstNum * secondNum;
    default:
      return "You didn't print any of the possible math operations";
  }
}

console.log(calcNUm(firstNumber, secondNumber, mathOperation));
