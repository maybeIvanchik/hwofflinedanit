"use strict";

function createNewUser() {
  let firstName = prompt("Enter your name");
  let lastName = prompt("Enter your lastName");
  let dateOfBirth = new Date(
    prompt("Enter here you date of birth in format (dd.mm.yyyy)")
      .split(".")
      .reverse()
      .join("-")
  );

  const newUser = {
    firstName: firstName,
    lastName: lastName,
  };

  newUser.getLogin = () => {
    return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase();
  };
  newUser.getAge = () => {
    debugger;
    return `${Math.floor(
      (new Date().getTime() - dateOfBirth) / (24 * 3600 * 365.25 * 1000)
    )} лет.`;
  };
  newUser.getPassword = () => {
    return (
      newUser.firstName[0].toUpperCase() +
      newUser.lastName.toLowerCase() +
      dateOfBirth.getFullYear()
    );
  };
  return newUser;
}

const myUser = createNewUser();
console.log(myUser.getLogin());
console.log(myUser.getPassword());
console.log(myUser.getAge());
