"use strict";

const tabs = document.querySelector(".tabs");
const tabsContent = document.querySelectorAll(".tabs-content-item");

tabs.addEventListener("click", (event) => {
  if (!event.target.closest(".tabs-title")) return;
  document.querySelectorAll(".tabs-title").forEach((elem) => {
    elem.classList.remove("active");
  });
  event.target.classList.add("active");
  tabsContent.forEach((elem) => {
    elem.classList.remove("active");
  });
  tabsContent[event.target.dataset.dataNum].classList.add("active");
});
