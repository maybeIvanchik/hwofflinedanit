"use strict";

const passwordType = document.querySelectorAll(".password");
const eyeOfPassword = document.querySelectorAll(".icon-password");
const errorAlert = document.querySelector(".password-error");
const form = document.querySelector(".password-form");

eyeOfPassword.forEach((eye, index) => {
  eye.addEventListener("click", () => {
    if (passwordType[index].type === "password") {
      passwordType[index].type = "text";
      eye.classList.add("fa-eye-slash");
    } else {
      passwordType[index].type = "password";
      eye.classList.remove("fa-eye-slash");
    }
  });
});

form.addEventListener("submit", (e) => {
  if (passwordType[0].value === passwordType[1].value) {
    e.target.reset();
    errorAlert.classList.remove("error");
    alert("You are welcome");
  } else {
    errorAlert.classList.add("error");
  }
  e.preventDefault();
});
