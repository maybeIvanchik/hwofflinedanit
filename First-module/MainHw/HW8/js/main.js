"use strict";

const parColor = document.querySelectorAll("p");
parColor.forEach((paragraph) => {
  paragraph.style.color = "#ff0000";
});

const optionsListElement = document.getElementById("optionsList");
console.log(optionsListElement);
console.log(optionsListElement.parentNode);
console.log(optionsListElement.childNodes);

const testPar = document.getElementById("testParagraph");
const testParOriginal = testPar.innerHTML;
testPar.innerHTML = "This is a paragraph";
//небыло класса testParagraph но был такой id - его и использовал

const mainHeaderCollection = document.querySelector(".main-header").children;
for (const child of mainHeaderCollection) {
  child.classList.add("nav-item");
  console.log(child);
}

const sectionTitleElements = document.querySelectorAll(".section-title");
sectionTitleElements.forEach((elem) => {
  elem.classList.remove("section-title");
});
// для последнего задание в коде HTML небыло классов section-title
//были добавлены мной в код
