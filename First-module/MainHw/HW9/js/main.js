"use strict";

const elementsCollection = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
];

const createListOfElements = (elements, parentEl) => {
  const bodyElement = document.querySelector(".body-page");

  elements.map((elem, index) => {
    elements[index] = `<li class="list-item-js">${elem}</li>`;
  });

  if (parentEl) {
    bodyElement.insertAdjacentHTML(
      "afterbegin",
      `<${parentEl} class = "container"><ul class="list-js">${elements.join(
        ""
      )}</ul></${parentEl}>`
    );
  } else {
    return bodyElement.insertAdjacentHTML(
      "afterbegin",
      `<ul class="list-js">${elements.join("")}</ul>`
    );
  }
};

createListOfElements(elementsCollection);
