"use strict";

let array = ["hello", "world", 23, "23", null, 23];

const filterBy = (arr, dataType) =>
  arr.filter((elem) => typeof elem !== dataType);

console.log(filterBy(array, "string"));
