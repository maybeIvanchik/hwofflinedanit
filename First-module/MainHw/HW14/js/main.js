"use strict";

const htmlPage = document.querySelector(".page");

const btnTheme = document.querySelector(".btn-site-theme");
btnTheme.addEventListener("click", () => {
  if (btnTheme.textContent === "Black Theme") {
    btnTheme.textContent = "Ligth Theme";
    btnTheme.classList.replace("active-black", "active-light");
    htmlPage.classList.replace("light", "black");
    localStorage.setItem("site theme", htmlPage.classList[1]);
  } else {
    btnTheme.textContent = "Black Theme";
    btnTheme.classList.replace("active-light", "active-black");
    htmlPage.classList.replace("black", "light");
    localStorage.setItem("site theme", htmlPage.classList[1]);
  }
});

if (localStorage.getItem("site theme") === "light") {
  htmlPage.classList.add("light");
  btnTheme.textContent = "Black Theme";
  btnTheme.classList.replace("active-light", "active-black");
} else {
  htmlPage.classList.add("black");
  btnTheme.textContent = "Ligth Theme";
  btnTheme.classList.replace("active-black", "active-light");
}
