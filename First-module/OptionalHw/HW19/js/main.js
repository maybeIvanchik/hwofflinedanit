"use strcit";

const devTasksInOneDay = [2, 1, 2, 1, 3];
const amountOfTask = [7, 10, 4, 12, 6];
const deadLineDate = new Date("2022-05-11");

const sprintPlaning = (devTasksInDay, amountOfTsk, dealineDt) => {
  let allTasksInOneDay = 0;
  let allTasks = 0;
  const daysToDeadline = Math.floor(
    Date.parse(deadLineDate) / 1000 / 60 / 60 / 24 -
      Date.parse(new Date()) / 1000 / 60 / 60 / 24 +
      1
  );

  devTasksInDay.forEach((element) => {
    allTasksInOneDay += element;
  });
  amountOfTsk.forEach((element) => {
    allTasks += element;
  });
  const taskForDay = allTasks - allTasksInOneDay;
  if (taskForDay <= daysToDeadline) {
    return `Усі завдання будуть успішно виконані за ${
      daysToDeadline - taskForDay
    } днів до настання дедлайну!`;
  }
  return `Команді розробників доведеться витратити додатково ${
    (taskForDay - daysToDeadline) * 24
  } годин після дедлайну, щоб виконати всі завдання в беклозі`;
};

console.log(sprintPlaning(devTasksInOneDay, amountOfTask, deadLineDate));
