"use strict";

const cloneOfObj = (obj) => {
  const clone = {};
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (typeof obj[key] === "object") {
        clone[key] = cloneOfObj(obj[key]);
      } else {
        clone[key] = obj[key];
      }
    }
  }
  return clone;
};

const user = {
  firstname: "John",
  lastname: "Smith",
  age: "22",
  table: {
    math: 12,
    informatik: 10,
    pe: 9,
  },
  sayHi() {
    console.log(`Вас приветствует ${this.firstname} ${this.lastname}!`);
  },
};

const userCLone = cloneOfObj(user);

console.log(user);
console.log(userCLone);
console.log(user === userCLone);
console.log(user.table === userCLone.table);
