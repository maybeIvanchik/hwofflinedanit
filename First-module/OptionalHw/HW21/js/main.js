"use strict";

document.querySelector(".circle-btn").addEventListener("click", (e) => {
  e.target.style.display = "none";
  document.body.insertAdjacentHTML(
    "afterbegin",
    `<div class="circle-div"></div>`
  );
  document
    .querySelector(".circle-div")
    .insertAdjacentHTML(
      "afterbegin",
      `<input class="circle-input" type="text" placeholder="Print here diametre"> <button class="circle-draw-btn">Намалювати</button>`
    );
  document.querySelector(".circle-draw-btn").addEventListener("click", (e) => {
    if (
      document.querySelector(".circle-input").value.length < 2 ||
      isNaN(document.querySelector(".circle-input").value)
    ) {
      e.preventDefault();
    } else {
      document.querySelector(".circle-div").style.display = "none";
      for (let i = 0; i < 100; i++) {
        document.body.insertAdjacentHTML(
          "beforeend",
          `<div class="circle"></div>`
        );
      }
    }
    const inputPx = document.querySelector(".circle-input").value;
    const circleColl = document.querySelectorAll(".circle");
    document.body.style.width = `${inputPx * 10}px`;
    circleColl.forEach((circle) => {
      circle.style.cssText = `border-radius: 1000px; width: ${inputPx}px; height: ${inputPx}px; background: rgb(${
        Math.random() * 255
      },${Math.random() * 255},${Math.random() * 255})`;
    });
    window.addEventListener("click", (e) => {
      if (e.target.closest(".circle")) {
        e.target.style.display = "none";
      }
    });
  });
});
