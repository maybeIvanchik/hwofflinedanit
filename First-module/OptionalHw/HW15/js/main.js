"use strict";
let userNum;

do {
  userNum = prompt("Напишите число факториал которого хотите узнать:", userNum);
} while (isNaN(userNum || userNum === "" || userNum === null));

const calcFactorial = (userNum) => {
  if (userNum === 0 || userNum === 1) {
    return 1;
  }
  return userNum * calcFactorial(userNum - 1);
};

console.log(calcFactorial(userNum));
