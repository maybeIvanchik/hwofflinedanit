"use strict";

document.body.insertAdjacentHTML("afterbegin", `<div class="table-div"></div>`);

for (let i = 0; i < 900; i++) {
  document
    .querySelector(".table-div")
    .insertAdjacentHTML("beforeend", `<div class="table-div-item"></div>`);
}

window.addEventListener("click", (e) => {
  if (e.target.closest(".table-div-item")) {
    e.target.classList.toggle("active");
  } else {
    document.documentElement.classList.toggle("change");
  }
});
