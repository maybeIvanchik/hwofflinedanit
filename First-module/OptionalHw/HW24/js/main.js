"use strict";

const minesweeperField = document.querySelector(".minesweeper-field");

const giveForBlockBomb = () => {
  let bombClass = [];
  for (let i = 0; i < 11; i++) {
    bombClass.push(Math.floor(Math.random() * 64));
  }
  return bombClass;
};
const showRestartGame = () => {
  document.body.classList.add("restart");
  document.body.insertAdjacentHTML(
    "afterbegin",
    `<div class="restart__container">
  <h2 class="restart__title">Game over</h2>
  <button class="restart__button">Restart Game</button>
</div>`
  );
};

window.addEventListener("load", () => {
  for (let i = 0; i < 64; i++) {
    minesweeperField.insertAdjacentHTML(
      "beforeend",
      `<button class="minesweeper-field__block hover"></button>`
    );
  }
  const minesweepreBlock = document.querySelectorAll(
    ".minesweeper-field__block"
  );
  let bombClassArray = giveForBlockBomb();
  for (let i = 0; i < 11; i++) {
    minesweepreBlock[bombClassArray[i]].classList.add("bomb");
  }
});

const nearBlocksPattern = [1, -1, 7, 8, 9, -7, -8, -9];

minesweeperField.addEventListener("click", (e) => {
  if (e.target.classList.contains("bomb")) {
    document.querySelectorAll(".minesweeper-field__block").forEach((elem) => {
      if (elem.classList.contains("bomb")) {
        elem.insertAdjacentHTML(
          "beforeend",
          `<img
                class="minesweeper-block__bomb"
                src="./img/pngegg.png"
                alt="bomb"
                width="30"
                height="30"
              />`
        );
      }
      elem.classList.replace("hover", "no-bomb");
    });
    setTimeout(() => {
      showRestartGame();
    }, 1500);
    window.addEventListener("click", (e) => {
      if (e.target.classList.contains("restart__button")) {
        document.location.reload(true);
      }
    });
  } else {
    const blocks = document.querySelectorAll(".minesweeper-field__block");
    if (e.target.classList.contains("no-bomb")) return;
    e.target.classList.replace("hover", "no-bomb");
    blocks.forEach((elem, index) => {
      if (elem === e.target) {
        let nearBlocks = [];
        for (let i = 0; i < 8; i++) {
          nearBlocks.push(index + nearBlocksPattern[i]);
        }
        let result = 0;
        try {
          for (let i = 0; i < 8; i++) {
            if (blocks[nearBlocks[i]].classList.contains("bomb")) {
              result += 1;
            }
          }
        } catch (err) {
          result += 0;
        }
        blocks[index].insertAdjacentHTML(
          "beforeend",
          `<p class ="number-bombs">${result}</p>`
        );
      }
    });
  }
});
