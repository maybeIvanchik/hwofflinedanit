"use strict";

const bodyInput = document.querySelector(".input");

bodyInput.addEventListener("blur", () => {
  if (bodyInput.value.length === 0) return;
  if (bodyInput.value == 0 || isNaN(bodyInput.value)) {
    bodyInput.classList.add("mistake");
    document
      .querySelector(".label")
      .insertAdjacentHTML(
        "afterend",
        `<span class="mistake-span">Please enter correct price</span>`
      );
  } else {
    document.querySelector(".label").insertAdjacentHTML(
      "beforebegin",
      `<div class="span-container">
        <span class="price_span">Поточsна ціна: $${bodyInput.value}</span>
        <img src="./img/icons8-delete-16.png" alt="delete" class="delete-img" />
      </div>`
    );
    document.querySelector(".delete-img").addEventListener("click", () => {
      document.querySelector(".span-container").remove();
      bodyInput.value = "";
    });
  }

  bodyInput.addEventListener("focus", () => {
    document.querySelector(".span-container").remove();
    bodyInput.classList.remove("mistake");
    document.querySelector(".mistake-span").remove();
  });
});
