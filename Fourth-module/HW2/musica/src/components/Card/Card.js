import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import style from "./Card.module.scss";

class Card extends PureComponent {
  render() {
    const {
      header,
      price,
      src,
      article,
      color,
      button,
      isFavorite,
      changedColor,
    } = this.props;

    return (
      <div className={style.Card} id={article}>
        <img src={src} alt="computer" className={style.img} />
        <p className={style.header}>{header}</p>
        <p className={style.price}>{price}</p>
        <div
          className={style.changedColor}
          style={{ backgroundColor: color }}
        ></div>

        <svg
          onClick={() => {changedColor(article)}}
          xmlns="http://www.w3.org/2000/svg"
          className={style.favorite}
          width="30"
          height="30"
          viewBox="0 0 24 24"
          fill={isFavorite ? "#C73434" : "#E0E0D0"}
        >
          <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
        </svg>
        {button}
      </div>
    );
  }
}

Card.propTypes = {
  header: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  color: PropTypes.string,
};

Card.defaultProps = {
  color: "black",
};

export default Card;
