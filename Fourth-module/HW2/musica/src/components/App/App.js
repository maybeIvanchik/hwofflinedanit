import React, { Component } from "react";
import Button from "../Button";
import style from "./App.module.scss";
import Modal from "../Modal";
import CardsList from "../CardsList";

class App extends Component {
  state = {
    isModalOpen: false,
    quantityInCart: 0,
    quantityInFavourite: 0,
    cards: [],
  };

  async componentDidMount() {
    const cards = localStorage.getItem("cards");
    const quantityInCart = localStorage.getItem("quantityInCart");
    const quantityInFavourite = localStorage.getItem("quantityInFavourite");

    if (cards) {
      this.setState({ cards: JSON.parse(cards) });
    } else {
      const cards = await fetch("./data.json").then((res) => res.json());
      localStorage.setItem("cards", JSON.stringify(cards));
      this.setState({ cards: cards });
    }

    if (quantityInCart) {
      this.setState({ quantityInCart: JSON.parse(quantityInCart) });
    }

    if (quantityInFavourite) {
      this.setState({ quantityInFavourite: JSON.parse(quantityInFavourite) });
    }
  }

  modalToogleClick = () => {
    this.setState((prevState) => ({
      isModalOpen: !prevState.isModalOpen,
    }));
  };

  AddOneMoreInBasket = () => {
    this.setState((prevState) => {
      const newState = { ...prevState };
      newState.quantityInCart += 1;

      localStorage.setItem("quantityInCart", newState.quantityInCart);
      return newState;
    });
    this.modalToogleClick();
  };

  addToFavorites = (article) => {
    this.setState((prevState) => {
      const newState = { ...prevState };
      const index = prevState.cards.findIndex(
        (card) => card.article === article
      );

      if (!newState.cards[index].isFavorite) {
        newState.cards[index].isFavorite = true;
        localStorage.setItem("cards", JSON.stringify(newState.cards));

        newState.quantityInFavourite += 1;
        localStorage.setItem(
          "quantityInFavourite",
          newState.quantityInFavourite
        );
      }

      return newState;
    });
  };

  render() {
    const { isModalOpen, quantityInCart, quantityInFavourite, cards } =
      this.state;
    return (
      <div className={style.App}>
        <div className={style.iconsContainer}>
          <div className={style.iconWrapper}>
            <img
              src="./img/icon/shopping-basket.png"
              alt="basket"
              width={30}
              height={30}
              className={style.basket}
            />
            <p className={style.quatity}>{quantityInCart}</p>
          </div>
          <div className={style.iconWrapper}>
            <svg
              className={style.favorite}
              xmlns="http://www.w3.org/2000/svg"
              width="30"
              height="30"
              fill="#C73434"
              viewBox="0 0 24 24"
            >
              <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
            </svg>
            <p className={style.quatity}>{quantityInFavourite}</p>
          </div>
        </div>
        <CardsList
          cards={cards}
          AddInBasketClick={this.modalToogleClick}
          changedColor={this.addToFavorites}
        />

        {isModalOpen && (
          <Modal
            header="Add Card in Basket?"
            closeButton={true}
            text="Do you really want add this product in basket?"
            closeModal={this.modalToogleClick}
            actions={
              <>
                <Button
                  text="Cancel"
                  backgroundColor="#621010"
                  onClick={this.modalToogleClick}
                />
                <Button
                  text="Ok"
                  backgroundColor="#621010"
                  onClick={this.AddOneMoreInBasket}
                />
              </>
            }
          />
        )}
      </div>
    );
  }
}

export default App;
