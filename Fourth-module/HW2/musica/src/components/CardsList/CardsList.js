import React, { Component } from "react";
import PropTypes from "prop-types";
import style from "./CardsList.module.scss";
import Button from "../Button";
import Card from "../Card";

class CardsList extends Component {
  render() {
    const { cards, AddInBasketClick, changedColor } = this.props;
    return (
      <ul className={style.CardList}>
        {cards.map(({ header, src, price, article, color, isFavorite }) => (
          <li key={article} className={style.CardItem}>
            <Card
              header={header}
              src={src}
              price={price}
              article={article}
              color={color}
              isFavorite={isFavorite}
              changedColor={changedColor}
              button={
                <Button
                  className={style.btn}
                  backgroundColor="#C73434"
                  text="Add to Card"
                  onClick={AddInBasketClick}
                />
              }
            />
          </li>
        ))}
      </ul>
    );
  }
}

CardsList.propTypes = {
  cards: PropTypes.array.isRequired,
  AddInBasketClick: PropTypes.func.isRequired,
  changedColor: PropTypes.func.isRequired
};

export default CardsList;
