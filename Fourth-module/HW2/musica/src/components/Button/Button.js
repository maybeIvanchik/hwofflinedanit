import React, { PureComponent } from 'react';
import style from "./Button.module.scss"
import PropTypes from "prop-types"

class Button extends PureComponent {
    render() {
        const { backgroundColor, text, onClick } = this.props;

        return (
            <button className={style.btn} style={{backgroundColor: backgroundColor}} onClick={onClick} >{text}</button>
        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    backgroundColor: "black",
};

export default Button;