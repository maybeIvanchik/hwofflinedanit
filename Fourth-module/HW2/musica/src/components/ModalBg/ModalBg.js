import React, { PureComponent } from 'react';
import style from "./ModalBg.module.scss"
import PropTypes from "prop-types"

class ModalBg extends PureComponent {
    render() {
        const {closeModal} = this.props;
        return (
            <div className={style.ModalBg} onClick={closeModal}/>
        );
    }
}

ModalBg.propTypes = {
    closeModal: PropTypes.func,
};

export default ModalBg;