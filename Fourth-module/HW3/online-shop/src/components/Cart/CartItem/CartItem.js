import PropTypes from "prop-types";
import style from "./CartItem.module.scss";

const CartItem = ({ header, src, article, price, button }) => {
  return (
    <>
      <div className={style.CartItem} id={article}>
        <img
          src={src}
          alt="cartImg"
          className={style.img}
          width="100"
          height="100"
        />
        <div className={style.info}>
          <div className={style.desc}>
            <p className={style.title}>{header}</p>
            <p className={style}>${price}</p>
          </div>
          <div className={style.btnBox}>{button}</div>
        </div>
      </div>
    </>
  );
};

CartItem.propTypes = {
  header: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  button: PropTypes.node.isRequired,
};

export default CartItem;
