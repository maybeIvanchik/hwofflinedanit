import PropTypes from "prop-types";
import style from "./CartList.module.scss";
import Button from "../../Button";
import CartItem from "../CartItem";

const CartList = ({ data, setCart, setIsModalOpen, setCartArticle }) => {
  if (data.length === 0) {
    return (
      <h1 className={style.title}>
        There are no items added in your cart yet.
      </h1>
    );
  }

  const priceAndCountInCartItem = (article, mode, price) => {
    const newCarts = [...data];
    const index = newCarts.findIndex((cart) => cart.article === article);

    if (mode === "+") {
      const num = +newCarts[index].allPrice + +price;
      newCarts[index].allPrice = num;
      newCarts[index].countInCart += 1;
    } else {
      if (newCarts[index].allPrice === +price) {
        return;
      } else {
        newCarts[index].allPrice -= price;
        newCarts[index].countInCart -= 1;
      }
    }
    setCart(newCarts);
    localStorage.setItem("cart", JSON.stringify(newCarts));
  };

  return (
    <section className={style.CartPage}>
      <h1 className={style.title}>Cart Page</h1>
      <ul className={style.CartList}>
        {data.map(({ header, src, allPrice, price, article, countInCart }) => (
          <li key={article}>
            <CartItem
              header={header}
              src={src}
              price={allPrice}
              article={article}
              button={
                <>
                  <div className={style.countBtnBox}>
                    <Button
                      backgroundColor="#C73434"
                      text="-"
                      onClick={() =>
                        priceAndCountInCartItem(article, "-", price)
                      }
                    />
                    <p>{countInCart}</p>
                    <Button
                      backgroundColor="#C73434"
                      text="+"
                      onClick={() =>
                        priceAndCountInCartItem(article, "+", price)
                      }
                    />
                  </div>
                  <Button
                    backgroundColor="#C73434"
                    text="Delete"
                    onClick={() => {
                      setIsModalOpen(true);
                      setCartArticle(article);
                    }}
                  />
                </>
              }
            />
          </li>
        ))}
      </ul>
    </section>
  );
};

CartList.propTypes = {
  data: PropTypes.array.isRequired,
  setCart: PropTypes.func.isRequired,
  setIsModalOpen: PropTypes.func.isRequired,
  setCartArticle: PropTypes.func.isRequired,
};

export default CartList;
