import style from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({ backgroundColor, onClick, text, disabled }) => {
  return (
    <button
      className={style.btn}
      style={{ backgroundColor: backgroundColor }}
      disabled={disabled}
      onClick={(e) => {
        onClick();
      }}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  backgroundColor: "black",
  disabled: false,
};

export default Button;
