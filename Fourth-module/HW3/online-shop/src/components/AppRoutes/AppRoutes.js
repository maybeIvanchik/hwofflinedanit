import { Routes, Route } from "react-router-dom";
import StorePage from "../../pages/StorePage";
import CartPage from "../../pages/CartPage";
import FavoritePage from "../../pages/FavoritePage";

const AppRoutes = (props) => {
  const {
    isModalOpen,
    quantityInCart,
    quantityInFavourite,
    cards,
    addToFavorites,
    AddOneMoreInBasket,
    setIsModalOpen,
    setConfirmInCart,
    setCartArticle,
    confirmInCart,
    cart,
    disableCartBtn,
    setCart,
    deleteFromCart,
    favoriteCard,
    deletFromFavorite
  } = props;

  return (
    <Routes>
      <Route
        path="/"
        element={
          <StorePage
            setIsModalOpen={setIsModalOpen}
            quantityInCart={quantityInCart}
            quantityInFavourite={quantityInFavourite}
            cards={cards}
            addToFavorites={addToFavorites}
            isModalOpen={isModalOpen}
            AddOneMoreInBasket={AddOneMoreInBasket}
            setConfirmInCart={setConfirmInCart}
            setCartArticle={setCartArticle}
            confirmInCart={confirmInCart}
            disableCartBtn={disableCartBtn}
          />
        }
      />

      <Route
        path="/cart"
        element={
          <CartPage
            cart={cart}
            setCart={setCart}
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
            deleteFromCart={deleteFromCart}
            setCartArticle={setCartArticle}
          />
        }
      />

      <Route path="/favorite" element={<FavoritePage favoriteCard={favoriteCard} deletFromFavorite={deletFromFavorite} />} />
    </Routes>
  );
};

export default AppRoutes;
