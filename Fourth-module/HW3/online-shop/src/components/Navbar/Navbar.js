import style from "./Navbar.module.scss";
import BasketIcon from "../svg/BasketIcon/BasketIcon";
import FavoriteIcon from "../svg/FavoriteIcon";
import classNames from "classnames";
import HomeIcon from "../svg/HomeIcon";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className={style.navbar}>
      <NavLink
        to="/"
        className={({ isActive }) => (!isActive ? style.icon : style.iconActive)}
      >
        <HomeIcon className={style.home} fill="#ffffff" />
      </NavLink>
      <NavLink
        to="/cart"
        className={({ isActive }) => (!isActive ? style.icon : style.iconActive)}
      >
        <BasketIcon
          className={classNames(style.basket, style.active)}
          fill="#ffffff"
        />
      </NavLink>
      <NavLink
        to="/favorite"
        className={({ isActive }) => (!isActive ? style.icon : style.iconActive)}
      >
        <FavoriteIcon fill="#ffffff" className={style.favorite} />
      </NavLink>
    </nav>
  );
};

export default Navbar;
