import PropTypes from "prop-types";
import style from "./FavoriteItem.module.scss";

const FavoriteItem = ({ src, article, price, button }) => {
  return (
    <>
      <div className={style.FavoriteItem} id={article}>
        <div className={style.bg}></div>
        <img
          src={src}
          alt="cartImg"
          className={style.img}
          width="200"
          height="200"
        />
        <p className={style.price}>${price}</p>
        {button}
      </div>
    </>
  );
};

FavoriteItem.propTypes = {
  src: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  button: PropTypes.node.isRequired,
};

export default FavoriteItem;
