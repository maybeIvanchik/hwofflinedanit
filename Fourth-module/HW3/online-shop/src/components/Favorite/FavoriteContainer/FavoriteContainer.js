import PropTypes from "prop-types";
import style from "./FavoriteContainer.module.scss";
import FavoriteItem from "../FavoriteItem";
import Button from "../../Button";

const FavoriteContainer = ({ data, deletFromFavorite }) => {
  if (data.length === 0) {
    return (
      <h1 className={style.title}>
        There are no items added in your favorite yet.
      </h1>
    );
  }

  return (
    <section className={style.FavoritePage}>
      <h1 className={style.title}>Favorite Page</h1>
      <ul className={style.FavoriteContainer}>
        {data.map(({ src, price, article, isFavorite }) => (
          <li key={article}>
            <FavoriteItem
              src={src}
              price={price}
              article={article}
              button={
                <Button
                  className={style.icon}
                  backgroundColor="#ba1b1b"
                  text="Delete from favorite"
                  onClick={() => deletFromFavorite(article)}
                />
              }
            />
          </li>
        ))}
      </ul>
    </section>
  );
};

FavoriteContainer.propTypes = {
  data: PropTypes.array.isRequired,
  deletFromFavorite: PropTypes.func.isRequired,
};

export default FavoriteContainer;
