import style from "./IconInfoContainer.module.scss";
import PropTypes from "prop-types";
import FavoriteIcon from "../svg/FavoriteIcon";
import BasketIcon from "../svg/BasketIcon/BasketIcon";

const IconInfoContainer = ({quantityInCart, quantityInFavourite}) => {
  return (
    <div className={style.iconsContainer}>
      <div className={style.iconWrapper}>
        <BasketIcon className={style.basket} fill="#000000"/>
        <p className={style.quatity}>{quantityInCart}</p>
      </div>
      <div className={style.iconWrapper}>
        <FavoriteIcon isFavorite={true} className={style.favorite} />
        <p className={style.quatity}>{quantityInFavourite}</p>
      </div>
    </div>
  );
};

IconInfoContainer.propTypes = {
    quantityInCart: PropTypes.number.isRequired,
    quantityInFavourite: PropTypes.number.isRequired
};

export default IconInfoContainer;
