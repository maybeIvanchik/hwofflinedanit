import React, { useEffect, useState } from "react";
import AppRoutes from "../AppRoutes";
import Navbar from "../Navbar";
import style from "./App.module.scss";
import IconInfoContainer from "../IconInfoContainer";

const App = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [quantityInCart, setQuantityInCart] = useState(0);
  const [quantityInFavourite, setQuantityInFavourite] = useState(0);
  const [cards, setCards] = useState([]);
  const [confirmInCart, setConfirmInCart] = useState(false);
  const [cartArticle, setCartArticle] = useState(0);
  const [cart, setCart] = useState([]);
  const [favoriteCard, setFavoriteCard] = useState([]);

  useEffect(() => {
    (async () => {
      const cards = localStorage.getItem("cards");
      const cart = localStorage.getItem("cart");
      const quantityInCart = localStorage.getItem("quantityInCart");
      const quantityInFavourite = localStorage.getItem("quantityInFavourite");
      const favoriteCards = localStorage.getItem("favoriteCards");

      if (cards) {
        setCards(JSON.parse(cards));
      } else {
        const cards = await fetch("./data.json").then((res) => res.json());
        localStorage.setItem("cards", JSON.stringify(cards));
        setCards(cards);
      }

      if (quantityInCart) {
        setQuantityInCart(JSON.parse(quantityInCart));
      }

      if (quantityInFavourite) {
        setQuantityInFavourite(JSON.parse(quantityInFavourite));
      }

      if (cart) {
        setCart(JSON.parse(cart));
      }

      if (favoriteCards) {
        setFavoriteCard(JSON.parse(favoriteCards));
      }
    })();
  }, []);

  useEffect(() => {
    if (confirmInCart) {
      const newCards = [...cards];
      const index = newCards.findIndex((card) => card.article === cartArticle);

      newCards[index].inCart = true;
      localStorage.setItem("cards", JSON.stringify(newCards));
      setCards(newCards);

      const newCart = [...cart];
      newCart.push(newCards[index]);
      localStorage.setItem("cart", JSON.stringify(newCart));
      setCart(newCart);

      setConfirmInCart(false);
    }
  }, [confirmInCart]);

  const AddOneMoreInBasket = () => {
    const countOfItemInBasket = quantityInCart + 1;
    setQuantityInCart(countOfItemInBasket);
    localStorage.setItem("quantityInCart", countOfItemInBasket);

    setIsModalOpen(false);
  };

  const deleteFromCart = () => {
    const newCart = [...cart];
    const index = newCart.findIndex((cart) => cart.article === cartArticle);

    newCart.splice(index, 1);
    localStorage.setItem("cart", JSON.stringify(newCart));
    setCart(newCart);
    setIsModalOpen(false);

    const newCards = [...cards];
    const indexOfCard = newCards.findIndex(
      (cart) => cart.article === cartArticle
    );
    newCards[indexOfCard].inCart = false;
    localStorage.setItem("cards", JSON.stringify(newCards));
    setCards(newCards);

    const countOfItemInBasket = quantityInCart - 1;
    setQuantityInCart(countOfItemInBasket);
    localStorage.setItem("quantityInCart", countOfItemInBasket);
  };

  const addToFavorites = (article) => {
    const newCards = [...cards];
    const index = newCards.findIndex((card) => card.article === article);

    if (!newCards[index].isFavorite) {
      newCards[index].isFavorite = true;
      localStorage.setItem("cards", JSON.stringify(newCards));
    } else {
      newCards[index].isFavorite = false;
      localStorage.setItem("cards", JSON.stringify(newCards));
    }
    const favoriteCount = newCards.filter((el) => el.isFavorite).length;
    localStorage.setItem("quantityInFavourite", favoriteCount);

    const favoriteArr = newCards.filter((el) => el.isFavorite);
    localStorage.setItem("favoriteCards", JSON.stringify(favoriteArr));

    setFavoriteCard(favoriteArr);
    setQuantityInFavourite(favoriteCount);
    setCards(newCards);
  };

  const deletFromFavorite = (article) => {
    const newCards = [...cards]
    const index = newCards.findIndex((card) => card.article === article)
    newCards[index].isFavorite = false;
    localStorage.setItem("cards", JSON.stringify(newCards))
    setCards(newCards)

    const newFavoritesArr = [...favoriteCard]
    const indexOfFavorite = newFavoritesArr.findIndex((card) => card.article === article)
    newFavoritesArr.splice(indexOfFavorite, 1)
    localStorage.setItem("favoriteCards", JSON.stringify(newFavoritesArr));
    setFavoriteCard(newFavoritesArr)
  };

  return (
    <div className={style.App}>
      <IconInfoContainer
        quantityInCart={quantityInCart}
        quantityInFavourite={quantityInFavourite}
      />
      <AppRoutes
        quantityInCart={quantityInCart}
        setIsModalOpen={setIsModalOpen}
        quantityInFavourite={quantityInFavourite}
        addToFavorites={addToFavorites}
        isModalOpen={isModalOpen}
        AddOneMoreInBasket={AddOneMoreInBasket}
        cards={cards}
        setConfirmInCart={setConfirmInCart}
        setCartArticle={setCartArticle}
        confirmInCart={confirmInCart}
        cart={cart}
        setCart={setCart}
        deleteFromCart={deleteFromCart}
        favoriteCard={favoriteCard}
        deletFromFavorite={deletFromFavorite}
      />
      <Navbar />
    </div>
  );
};

export default App;
