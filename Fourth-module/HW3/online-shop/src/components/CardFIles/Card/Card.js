import PropTypes from "prop-types";
import style from "./Card.module.scss";
import FavoriteIcon from "../../svg/FavoriteIcon";

const Card = ({
  header,
  price,
  src,
  article,
  color,
  button,
  isFavorite,
  changedColor,
}) => {
  return (
    <div className={style.Card} id={article}>
      <img src={src} alt="computer" className={style.img} />
      <p className={style.header}>{header}</p>
      <p className={style.price}>${price}</p>
      <div
        className={style.changedColor}
        style={{ backgroundColor: color }}
      ></div>
      <FavoriteIcon
        article={article}
        changedColor={changedColor}
        isFavorite={isFavorite}
        className={style.favorite}
      />
      {button}
    </div>
  );
};

Card.propTypes = {
  header: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  color: PropTypes.string,
  isFavorite: PropTypes.bool.isRequired,
  changedColor: PropTypes.func.isRequired,
};

Card.defaultProps = {
  color: "black",
};

export default Card;
