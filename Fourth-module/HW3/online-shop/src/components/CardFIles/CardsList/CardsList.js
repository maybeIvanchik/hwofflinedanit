import PropTypes from "prop-types";
import style from "./CardsList.module.scss";
import Button from "../../Button";
import Card from "../Card";

const CardsList = ({ data, changedColor, setIsModalOpen, setCartArticle }) => {
  return (
    <ul className={style.CardList}>
      {data.map(
        ({ header, src, price, article, color, isFavorite, inCart }) => (
          <li key={article} className={style.CardItem}>
            <Card
              header={header}
              src={src}
              price={price}
              article={article}
              color={color}
              isFavorite={isFavorite}
              changedColor={changedColor}
              button={
                <Button
                  className={style.btn}
                  backgroundColor="#C73434"
                  disabled={inCart ? true : false}
                  text={inCart ? "Added" : "Add in Cart"}
                  onClick={(e) => {
                    setCartArticle(article);
                    setIsModalOpen(true);
                  }}
                />
              }
            />
          </li>
        )
      )}
    </ul>
  );
};

CardsList.propTypes = {
  data: PropTypes.array.isRequired,
  AddInBasketClick: PropTypes.func.isRequired,
  changedColor: PropTypes.func.isRequired,
  setCartArticle: PropTypes.func.isRequired,
};

export default CardsList;
