import style from "./ModalBg.module.scss";
import PropTypes from "prop-types";

const ModalBg = ({ closeModal }) => {
  return <div className={style.ModalBg} onClick={closeModal} />;
};

ModalBg.propTypes = {
  closeModal: PropTypes.func,
};

export default ModalBg;
