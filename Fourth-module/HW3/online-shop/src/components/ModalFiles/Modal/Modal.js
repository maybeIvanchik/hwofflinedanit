import style from "./Modal.module.scss";
import ModalBg from "../ModalBg";
import PropTypes from "prop-types";

const Modal = ({
  header,
  closeButton,
  text,
  actions,
  isModalOpen,
  setIsModalOpen,
}) => {
  const closeModal = () => {
    setIsModalOpen(false);
  };

  if (!isModalOpen) {
    return null;
  }

  return (
    <>
      <ModalBg closeModal={closeModal} />
      <div className={style.Modal}>
        <h2 className={style.title}>{header}</h2>
        <p className={style.text}>{text}</p>
        {closeButton && (
          <div className={style.closeBtn} onClick={closeModal}></div>
        )}
        <div className={style.footer}>{actions}</div>
      </div>
    </>
  );
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  text: PropTypes.string.isRequired,
  actions: PropTypes.node,
  isModalOpen: PropTypes.bool.isRequired,
  setIsModalOpen: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  closeButton: false,
  actions: "",
};

export default Modal;
