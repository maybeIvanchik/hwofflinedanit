import PropTypes from "prop-types";

const FavoriteIcon = ({
  article,
  isFavorite,
  changedColor,
  className,
  fill,
}) => {
  return (
    <svg
      className={className}
      onClick={() => {
        changedColor(article);
      }}
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      viewBox="0 0 24 24"
      fill={fill ? fill : isFavorite ? "#C73434" : "#D0D0D0"}
    >
      <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
    </svg>
  );
};

FavoriteIcon.propTypes = {
  article: PropTypes.string,
  isFavorite: PropTypes.bool,
  changedColor: PropTypes.func,
  className: PropTypes.string,
  fill: PropTypes.string,
};

FavoriteIcon.defaultProps = {
  article: null,
  isFavorite: false,
  changedColor: () => {},
  className: "",
};

export default FavoriteIcon;
