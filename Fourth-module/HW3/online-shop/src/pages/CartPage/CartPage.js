import CartList from "../../components/Cart/CartList";
import Modal from "../../components/ModalFiles/Modal";
import Button from "../../components/Button";

const CartPage = ({ cart, setCart, isModalOpen, setIsModalOpen, deleteFromCart, setCartArticle }) => {
  return (
    <div>
      <CartList data={cart} setCart={setCart} setIsModalOpen={setIsModalOpen} setCartArticle={setCartArticle} />
      <Modal
        header="Delete thisItem from Basket"
        closeButton={true}
        text="Do you really want add this product in basket?"
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        actions={
          <>
            <Button
              text="Cancel"
              backgroundColor="#621010"
              onClick={() => setIsModalOpen(false)}
            />
            <Button text="Ok" backgroundColor="#621010" onClick={deleteFromCart} />
          </>
        }
      />
    </div>
  );
};

export default CartPage;
