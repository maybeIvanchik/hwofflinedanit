import Button from "../../components/Button";
import style from "./StorePage.module.scss";
import Modal from "../../components/ModalFiles/Modal";
import CardsList from "../../components/CardFIles/CardsList";

const StorePage = (props) => {
  const {
    isModalOpen,
    setIsModalOpen,
    cards,
    addToFavorites,
    AddOneMoreInBasket,
    setConfirmInCart,
    setCartArticle,
    confirmInCart,
    disableCartBtn,
  } = props;

  return (
    <div className={style.StorePage}>
      <h1 className={style.title}>Home Page</h1>
      <CardsList
        data={cards}
        AddInBasketClick={AddOneMoreInBasket}
        changedColor={addToFavorites}
        setIsModalOpen={setIsModalOpen}
        setCartArticle={setCartArticle}
        confirmInCart={confirmInCart}
        disableCartBtn={disableCartBtn}
      />
      <Modal
        header="Add Card in Basket?"
        closeButton={true}
        text="Do you really want add this product in basket?"
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        actions={
          <>
            <Button
              text="Cancel"
              backgroundColor="#621010"
              onClick={() => setIsModalOpen(false)}
            />
            <Button
              text="Ok"
              backgroundColor="#621010"
              onClick={(e) => {
                setConfirmInCart(true);
                AddOneMoreInBasket();
              }}
            />
          </>
        }
      />
    </div>
  );
};

export default StorePage;
