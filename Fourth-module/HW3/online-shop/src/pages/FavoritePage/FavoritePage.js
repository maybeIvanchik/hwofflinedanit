import FavoriteContainer from "../../components/Favorite/FavoriteContainer"

const FavoritePage = ({favoriteCard, deletFromFavorite}) => {
    return (
      <div>
        <FavoriteContainer data={favoriteCard} deletFromFavorite={deletFromFavorite}/>
      </div>
    );
  };
  
  export default FavoritePage;