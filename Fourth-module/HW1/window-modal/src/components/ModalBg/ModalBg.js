import React, { Component } from 'react';
import style from "./ModalBg.module.scss"

class ModalBg extends Component {
    render() {
        const {closeModal} = this.props;
        return (
            <div className={style.ModalBg} onClick={closeModal}/>
        );
    }
}

export default ModalBg;