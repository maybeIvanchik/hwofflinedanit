import React, { Component } from "react";
import style from "./Modal.module.scss";
import ModalBg from "../ModalBg";

class Modal extends Component {
  render() {
    const { header, closeButton, text, actions, closeModal } = this.props;
    return (
      <>
        <ModalBg closeModal={closeModal} />
        <div className={style.Modal}>
          <h2 className={style.title}>{header}</h2>
          <p className={style.text}>{text}</p>
          {closeButton && <div className={style.closeBtn} onClick={closeModal}></div>}
          <div className={style.footer}>{actions}</div>
        </div>
      </>
    );
  }
}

export default Modal;
