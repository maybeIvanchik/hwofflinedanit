import React, { Component } from "react";
import Button from "../Button";
import style from "./App.module.scss";
import Modal from "../Modal";

class App extends Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  };
  
  firstModalToogle = () => {
    this.setState((prevState) => ({
      isFirstModalOpen: !prevState.isFirstModalOpen,
    }));
  };
  SecondModalToogle = () => {
    this.setState((prevState) => ({
      isSecondModalOpen: !prevState.isSecondModalOpen,
    }));
  };

  render() {
    const { isFirstModalOpen, isSecondModalOpen } = this.state;
    return (
      <div className="App">
        <div className={style.btnWrapper}>
          <Button
            backgroundColor="red"
            text="Open modal 1"
            onClick={this.firstModalToogle}
          />
          <Button
            backgroundColor="blue"
            text="Open modal 2"
            onClick={this.SecondModalToogle}
          />
        </div>
        {isFirstModalOpen && (
          <Modal
            header="Do you want to delete this file?"
            text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it"
            actions = {
              <>
                <Button
                  backgroundColor="black"
                  text="Cancel"
                  onClick={this.firstModalToogle}
                />
                <Button
                  backgroundColor="black"
                  text="Ok"
                  onClick={this.firstModalToogle}
                />
              </>
            }
            closeButton={true}
            closeModal={this.firstModalToogle}
          />
        )}
        {isSecondModalOpen && <Modal
            header="Do you want to do someting?"
            text="Here you can delete someting"
            actions = {
              <>
                <Button
                  backgroundColor="black"
                  text="Cancel"
                  onClick={this.SecondModalToogle}
                />
                <Button
                  backgroundColor="black"
                  text="Ok"
                  onClick={this.SecondModalToogle}
                />
              </>
            }
            closeButton={false}
            closeModal={this.SecondModalToogle}
          />}
      </div>
    );
  }
}

export default App;
