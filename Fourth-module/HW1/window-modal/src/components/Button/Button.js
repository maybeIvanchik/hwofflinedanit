import React, { Component } from 'react';
import style from "./Button.module.scss"

class Button extends Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;

        return (
            <button className={style.btn} style={{backgroundColor: backgroundColor}} onClick={onClick} >{text}</button>
        );
    }
}

export default Button;